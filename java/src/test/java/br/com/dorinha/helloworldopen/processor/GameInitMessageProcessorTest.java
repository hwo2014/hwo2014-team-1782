package br.com.dorinha.helloworldopen.processor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import br.com.dorinha.helloworldopen.domain.Car;
import br.com.dorinha.helloworldopen.domain.Game;
import br.com.dorinha.helloworldopen.domain.Lane;
import br.com.dorinha.helloworldopen.domain.Piece;
import br.com.dorinha.helloworldopen.domain.Race;
import br.com.dorinha.helloworldopen.domain.StartingPoint;
import br.com.dorinha.helloworldopen.domain.Track;

import com.google.gson.JsonParser;

public class GameInitMessageProcessorTest {

	@Test
	public void testProcessor() {
		Game game = new Game();

		String json = " {\"msgType\": \"gameInit\", \"data\": { \"race\": { \"track\": { \"id\": \"indianapolis\", \"name\": \"Indianapolis\", \"pieces\": [ { \"length\": 100.0 }, { \"length\": 100.0, \"switch\": true }, { \"radius\": 200, \"angle\": 22.5 } ], \"lanes\": [ { \"distanceFromCenter\": -20, \"index\": 0 }, { \"distanceFromCenter\": 0, \"index\": 1 }, { \"distanceFromCenter\": 20, \"index\": 2 } ], \"startingPoint\": { \"position\": { \"x\": -340.0, \"y\": -96.0 }, \"angle\": 90.0 } }, \"cars\": [ { \"id\": { \"name\": \"Schumacher\", \"color\": \"red\" }, \"dimensions\": { \"length\": 40.0, \"width\": 20.0, \"guideFlagPosition\": 10.0 } }, { \"id\": { \"name\": \"Rosberg\", \"color\": \"blue\" }, \"dimensions\": { \"length\": 40.0, \"width\": 20.0, \"guideFlagPosition\": 10.0 } } ], \"raceSession\": { \"laps\": 3, \"maxLapTimeMs\": 30000, \"quickRace\": true } } }}";

		new GameInitMessageProcessor().process(game, new JsonParser().parse(json).getAsJsonObject(),
				ServerMessageType.GAMEINIT);

		Race race = game.race();
		assertEquals(3, race.getLaps());
		assertEquals(30000l, race.getMaxLapTimeMs());
		assertTrue(race.isQuickRace());

		Track track = race.getTrack();
		assertEquals("indianapolis", track.getId());

		List<Piece> pieces = track.getPieces();
		assertEquals(3, pieces.size());

		assertEquals(100.0, pieces.get(0).getLength(), 0.001);

		assertEquals(100.0, pieces.get(1).getLength(), 0.001);
		assertTrue(pieces.get(1).hasSwitch());

		assertEquals(200.0, pieces.get(2).getRadius(), 0.001);
		assertEquals(22.5, pieces.get(2).getAngle(), 0.001);

		List<Lane> lanes = track.getLanes();
		assertEquals(3, lanes.size());

		assertEquals(-20.0, lanes.get(0).getDistanceFromCenter(), 0.001);
		assertEquals(0, lanes.get(0).getIndex());

		assertEquals(0.0, lanes.get(1).getDistanceFromCenter(), 0.001);
		assertEquals(1, lanes.get(1).getIndex());

		assertEquals(20.0, lanes.get(2).getDistanceFromCenter(), 0.001);
		assertEquals(2, lanes.get(2).getIndex());

		StartingPoint startingPoint = track.getStartingPoint();
		assertEquals(-340.0, startingPoint.getX(), 0.001);
		assertEquals(-96.0, startingPoint.getY(), 0.001);
		assertEquals(90.0, startingPoint.getAngle(), 0.001);

		Car car = game.getCar("red");
		assertEquals(40.0, car.getLength(), 0.001);
		assertEquals(20.0, car.getWidth(), 0.001);
		assertEquals(10.0, car.getGuideFlagPosition(), 0.001);

		car = game.getCar("blue");
		assertEquals(40.0, car.getLength(), 0.001);
		assertEquals(20.0, car.getWidth(), 0.001);
		assertEquals(10.0, car.getGuideFlagPosition(), 0.001);
	}
}
