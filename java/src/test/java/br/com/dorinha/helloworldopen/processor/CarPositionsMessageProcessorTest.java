package br.com.dorinha.helloworldopen.processor;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.com.dorinha.helloworldopen.domain.Car;
import br.com.dorinha.helloworldopen.domain.Game;
import br.com.dorinha.helloworldopen.domain.Track;

import com.google.gson.JsonParser;

public class CarPositionsMessageProcessorTest {

	@Test
	public void testProcessor() {
		Game game = new Game();
		game.init();
		game.start();

		String json = "{\"msgType\": \"yourCar\", \"data\": {  \"name\": \"Schumacher\",  \"color\": \"red\"}}";

		new YourCarMessageProcessor().process(game, new JsonParser().parse(json).getAsJsonObject(),
				ServerMessageType.YOURCAR);

		json = "{\"msgType\": \"gameInit\", \"data\": { \"race\": { \"track\": { \"id\": \"indianapolis\", \"name\": \"Indianapolis\", \"pieces\": [ { \"length\": 100.0 }, { \"length\": 100.0, \"switch\": true }, { \"radius\": 200, \"angle\": 22.5 } ], \"lanes\": [ { \"distanceFromCenter\": -20, \"index\": 0 }, { \"distanceFromCenter\": 0, \"index\": 1 }, { \"distanceFromCenter\": 20, \"index\": 2 } ], \"startingPoint\": { \"position\": { \"x\": -340.0, \"y\": -96.0 }, \"angle\": 90.0 } }, \"cars\": [ { \"id\": { \"name\": \"Schumacher\", \"color\": \"red\" }, \"dimensions\": { \"length\": 40.0, \"width\": 20.0, \"guideFlagPosition\": 10.0 } }, { \"id\": { \"name\": \"Rosberg\", \"color\": \"blue\" }, \"dimensions\": { \"length\": 40.0, \"width\": 20.0, \"guideFlagPosition\": 10.0 } } ], \"raceSession\": { \"laps\": 3, \"maxLapTimeMs\": 30000, \"quickRace\": true } } }}";

		new GameInitMessageProcessor().process(game, new JsonParser().parse(json).getAsJsonObject(),
				ServerMessageType.GAMEINIT);

		json = "{\"msgType\": \"carPositions\", \"data\": [ { \"id\": { \"name\": \"Schumacher\", \"color\": \"red\" }, \"angle\": 0.0, \"piecePosition\": { \"pieceIndex\": 0, \"inPieceDistance\": 0.0, \"lane\": { \"startLaneIndex\": 0, \"endLaneIndex\": 0 }, \"lap\": 0 } }, { \"id\": { \"name\": \"Rosberg\", \"color\": \"blue\" }, \"angle\": 45.0, \"piecePosition\": { \"pieceIndex\": 0, \"inPieceDistance\": 20.0, \"lane\": { \"startLaneIndex\": 1, \"endLaneIndex\": 1 }, \"lap\": 0 } } ], \"gameId\": \"OIUHGERJWEOI\", \"gameTick\": 0}";

		new CarPositionsMessageProcessor().process(game, new JsonParser().parse(json).getAsJsonObject(),
				ServerMessageType.GAMEINIT);

		Car car = game.getCar("red");
		assertEquals(0.0, car.getAngle(), 0.001);
		assertEquals(0, car.getLap());

		car = game.getCar("blue");
		assertEquals(45.0, car.getAngle(), 0.001);
		assertEquals(0, car.getLap());
	}

	@Test
	public void testProcessCar() {
		String json = "{\"id\":{\"name\":\"dorinha\",\"color\":\"red\"},\"angle\":0.0,\"piecePosition\":{\"pieceIndex\":0,\"inPieceDistance\":0.0,\"lane\":{\"startLaneIndex\":0,\"endLaneIndex\":0},\"lap\":0}}";
		Car car = new Car("red", new Game());
		Track track = new Track();
		track.createPiece();
		track.createLane();
		new CarPositionsMessageProcessor().processCar(car, track, new JsonParser().parse(json).getAsJsonObject());
	}
	
}