package br.com.dorinha.helloworldopen.processor;

import static org.junit.Assert.*;

import org.junit.Test;

import br.com.dorinha.helloworldopen.domain.Car;
import br.com.dorinha.helloworldopen.domain.Game;
import static br.com.dorinha.helloworldopen.utils.JsonObjectBuilder.*;

public class YourCarMessageProcessorTest {

	@Test
	public void testProcessor() {
		Game game = new Game();

		new YourCarMessageProcessor().process(game,
				json().with("msgType", "yourCar").with("data", json().with("name", "Schumacher").with("color", "red"))
						.toJson(), ServerMessageType.YOURCAR);

		Car car = game.getMyCar();

		assertEquals("red", car.getColor());
	}
}
