package br.com.dorinha.helloworldopen.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CarTest {

	@Test
	public void testVelocitySingleLane() {
		Game game = new Game();
		Piece piece1 = game.race().track().createPiece().setAngle(0).setLength(100);
		Piece piece2 = game.race().track().createPiece().setAngle(0).setLength(100);
		Piece piece3 = game.race().track().createPiece().setAngle(30).setRadius(25);
		Piece piece4 = game.race().track().createPiece().setAngle(45).setRadius(25);
		Lane lane = game.race().track().createLane();

		Car car = new Car("red", game);

		Position p1 = new Position(piece1, lane, lane, 10.0, 0);
		sleep(100);
		Position p2 = new Position(piece2, lane, lane, 20.0, 0);
		sleep(100);
		Position p3 = new Position(piece2, lane, lane, 50.0, 0);
		sleep(100);
		Position p4 = new Position(piece3, lane, lane, 10.0, 0);
		sleep(100);
		Position p5 = new Position(piece3, lane, lane, 10.0, 0);
		sleep(100);
		Position p6 = new Position(piece4, lane, lane, 25.0, 0);

		assertTrue(between(car.calculateVelocity(p1, p2), 1.06, 1.4));
		assertTrue(between(car.calculateVelocity(p2, p3), 0.26, 0.34));
		assertTrue(between(car.calculateVelocity(p3, p4), 0.45, 0.65));
		assertEquals(0.0, car.calculateVelocity(p4, p5), 0.001);
		assertTrue(between(car.calculateVelocity(p5, p6), 0.25, 0.35));
	}

	private boolean between(double v, double i, double e) {
		return i < v && v < e;
	}

	private void sleep(long t) {
		try {
			Thread.sleep(t);
		} catch (InterruptedException e) {
		}
	}
}
