package br.com.dorinha.helloworldopen.domain;

import java.io.File;

import org.junit.Test;

public class KnowledgeBaseTest {

	@Test
	public void testBuildKnowledgeBase() {
		KnowledgeBase base = new KnowledgeBase("/tmp/store." + System.currentTimeMillis() + ".data");
		base.createKnowledge(0, 0, 1, 45, new PieceAngleForecasts(0, 0, 0, 45, 45), new SwitchLanePotentials(0, 1, 0),
				0.6, SwitchLaneDecision.Stay);
		base.createKnowledge(0, 0, 1, 45, new PieceAngleForecasts(0, 0, 0, 45, 45), new SwitchLanePotentials(0, 1, 0),
				0.6, SwitchLaneDecision.Stay);
		base.createKnowledge(0, 0, 1, 45, new PieceAngleForecasts(0, 0, 0, 45, 45), new SwitchLanePotentials(0, 1, 0),
				0.6, SwitchLaneDecision.Stay);
		base.createKnowledge(0, 0, 1, 45, new PieceAngleForecasts(0, 0, 0, 45, 45), new SwitchLanePotentials(0, 1, 0),
				0.6, SwitchLaneDecision.Stay);
		base.notifyLapResult(0, 0.6);
		base.persist();
	}

	@Test
	public void testLoadAndUpdateKnowledgeBase() {
		new File("/tmp/store.data").delete();
		KnowledgeBase base = new KnowledgeBase("/tmp/store.data");
		base.createKnowledge(0, 0, 1, 45, new PieceAngleForecasts(0, 0, 0, 45, 45), new SwitchLanePotentials(0, 1, 0),
				0.6, SwitchLaneDecision.Stay);
		base.createKnowledge(0, 0, 1, 45, new PieceAngleForecasts(0, 0, 0, 45, 45), new SwitchLanePotentials(0, 1, 0),
				0.6, SwitchLaneDecision.Stay);
		base.createKnowledge(0, 0, 1, 45, new PieceAngleForecasts(0, 0, 0, 45, 45), new SwitchLanePotentials(0, 1, 0),
				0.6, SwitchLaneDecision.Stay);
		base.createKnowledge(0, 0, 1, 45, new PieceAngleForecasts(0, 0, 0, 45, 45), new SwitchLanePotentials(0, 1, 0),
				0.6, SwitchLaneDecision.Stay);
		base.notifyLapResult(0, 0.6);
		base.persist();

		base = new KnowledgeBase("/tmp/store.data");
		base.createKnowledge(0, 0, 1, 45, new PieceAngleForecasts(0, 0, 0, 45, 45), new SwitchLanePotentials(0, 1, 0),
				0.4, SwitchLaneDecision.Stay);
		base.createKnowledge(0, 0, 1, 45, new PieceAngleForecasts(0, 0, 0, 45, 45), new SwitchLanePotentials(0, 1, 0),
				0.5, SwitchLaneDecision.Stay);
		base.createKnowledge(0, 0, 1, 45, new PieceAngleForecasts(0, 0, 0, 45, 45), new SwitchLanePotentials(0, 1, 0),
				0.4, SwitchLaneDecision.Stay);
		base.createKnowledge(0, 0, 1, 45, new PieceAngleForecasts(0, 0, 0, 45, 45), new SwitchLanePotentials(0, 1, 0),
				0.43, SwitchLaneDecision.Stay);
		base.notifyLapResult(0, 0.8);
		base.persist();
	}
}
