package br.com.dorinha.helloworldopen.domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class PieceTest {

	@Test
	public void testGetArcLength() {
		Piece piece = new Piece().setAngle(30).setRadius(2);
		assertEquals(1.047, piece.getArcLength(), 0.001);
	}
}
