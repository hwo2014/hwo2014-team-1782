package br.com.dorinha.helloworld.mlp.network;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestFunction {

	@Test
	public void testCalculateSigmoid() {
		assertEquals(.6704011598088686d, Function.calculateSigmoid(.71d), 0.001);
	}

	@Test
	public void testCalculateSigmoidDerivative() {
		assertEquals(.22096344473579244d, Function.calculateSigmoidDerivative(.6704011598088686d), 0.001);
	}

}