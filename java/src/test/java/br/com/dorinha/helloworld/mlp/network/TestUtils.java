package br.com.dorinha.helloworld.mlp.network;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

public class TestUtils {

	@Test
	public void testGetNewVector() {
		Double[] expect = { 0d, 0d, 0d };
		Double[] actual = Utils.getNewVector(3);
		Assert.assertArrayEquals(expect, actual);
	}

	@Test
	public void testCalcuteSigmoidFunction() {
		Double[] expect = new Double[] { .6704011598088686d, .617747874769249d };
		Double[] actual = Utils.calcuteSigmoidFunction(new Double[] { .71d, .48d });
		Assert.assertArrayEquals(expect, actual);
	}

	@Test
	public void testCalculateDiff() {
		Double[] v1 = new Double[] { 1d, 1d };
		Double[] v2 = new Double[] { 1d, 1d };
		Double actual = Utils.calculateDiff(v1, v2);
		assertEquals(0d, actual, 0.001);
	}

}