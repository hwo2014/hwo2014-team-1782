package br.com.dorinha.helloworld.mlp.learning;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.dorinha.helloworld.mlp.learning.BackPropagation;

public class TestBackPropagation {

	public static final Double LEARNING_RATE = .8d;
	private BackPropagation backPropagation;
	
	@Before
	public void setUp() {
		this.backPropagation = new BackPropagation(TestBackPropagation.LEARNING_RATE);
	}

	@Test
	public void testCalculateOutputMiddleLayer() {
		Double[] inputPattern = { 0d, 0d, 1d };
		Double[][] weightsMiddleLayer = {
			{.42d, .32d},
			{.27d, .91d},
			{.71d, .48d}
		};
		Double[] actual = this.backPropagation.calculateOutputMiddleLayer(inputPattern, weightsMiddleLayer);
		Double[] expect = { .6704011598088686d, .617747874769249d };
		Assert.assertArrayEquals(expect, actual);
	}

	@Test
	public void testCalculateOutputLastLayer() {
		Double[] outputMiddleLayer = { .6704011598088686d, .617747874769249d, 1d };
		Double[][] lastLayerWeights = {
			{.63d},
			{.55d},
			{.21d}
		};
		Double[] actual = this.backPropagation.calculateOutputLastLayer(outputMiddleLayer, lastLayerWeights);
		Assert.assertArrayEquals(new Double[] { .9721140618026742d }, actual);
	}

	@Test
	public void testCalculateOutputLastLayerError() {
		Double[] wantedValue = new Double[] { 0d };
		Double[] obtainedValue = new Double[] { .9721140618026742d };
		Double[] actual = this.backPropagation.calculateOutputLastLayerError(wantedValue, obtainedValue);
		Double[] expect = new Double[] { .9721140618026742d };
		Assert.assertArrayEquals(expect, actual);
	}

	@Test
	public void testRefreshLastLayerWeights() {
		Double[] outputLastLayer = { .9721140618026742d };
		Double[][] expectWeightsLastLayer = new Double[][] { { -.12600459932359487d }, { -.20600459932359483d }, { -.5460045993235949d } };
		Double[][] lastLayerWeights = {
			{.63d},
			{.55d},
			{.21d}
		};
		Double[] error = new Double[] { .9721140618026742d };
		this.backPropagation.refreshLastLayerWeights(outputLastLayer, error, lastLayerWeights);
		Assert.assertArrayEquals(expectWeightsLastLayer, lastLayerWeights);
	}
	
	@Test
	public void testCalculateAdjustmentFactors() {
		Double[] expect = { .1353250532163738d, .12625281886657674d, 0d };
		Double[] outputMiddleLayer = { .6704011598088686d, .617747874769249d, 1d };
		Double[][] middleLayerWeights = {
			{ .63d }, 
			{ .55d }, 
			{ .21d }
		};
		Double[] error = { .9721140618026742d };
		Double[] actual = this.backPropagation.calculateAdjustmentFactors(outputMiddleLayer , middleLayerWeights , error);
		Assert.assertArrayEquals(expect, actual);
	}
	
	@Test
	public void testRefreshMiddleLayerWeights() {
		Double[][] expectMiddleLayerWeights = new Double[][] {
			{ .420d, .320d },
			{ .270d, .910d },
			{ .6017399574269009d, .3789977449067386d }
		};
		Double[][] middleLayerWeights = {
			{.42d, .32d},
			{.27d, .91d},
			{.71d, .48d}
		};
		Double[][] lastLayerWeights = {
			{.63d},
			{.55d},
			{.21d}
		};
		Double[] input = { 0d, 0d, 1d };
		Double[] outputMiddleLayer = { .6704011598088686d, .617747874769249d, 1d };
		Double[] error = { .9721140618026742d };
		this.backPropagation.refreshMiddleLayerWeights(input, middleLayerWeights, outputMiddleLayer, lastLayerWeights, error);
		Assert.assertArrayEquals(expectMiddleLayerWeights, middleLayerWeights);
	}

}