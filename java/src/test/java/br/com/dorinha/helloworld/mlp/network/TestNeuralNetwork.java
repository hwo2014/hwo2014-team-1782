package br.com.dorinha.helloworld.mlp.network;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import br.com.dorinha.helloworld.mlp.learning.BackPropagation;
import br.com.dorinha.helloworld.mlp.learning.LearningAlgorithm;
import br.com.dorinha.helloworld.mlp.learning.TestBackPropagation;

public class TestNeuralNetwork {

	private static final Double SUPPORTED_ERROR = .1d;
	private NeuralNetwork neuralNetwork;

	@Before
	public void setUp() {
		Topology topology = new Topology(2, 2, 1);
		LearningAlgorithm learningAlgorithm = new BackPropagation(TestBackPropagation.LEARNING_RATE);
		this.neuralNetwork = new NeuralNetwork(topology, learningAlgorithm, TestNeuralNetwork.SUPPORTED_ERROR);
	}

	@Test
	public void testAssignRandomWeights() {
		Double[][] weights = new Double[3][4];
		this.neuralNetwork.assignRandomWeights(weights);
		for (int i = 0; i < weights.length; i++) {
			for (int j = 0; j < weights[i].length; j++) {
				Double value = weights[i][j];
				assertTrue(value >= -.5 && value <= .5);
			}
		}
	}

	@Test
	public void testIsNotDifferent() {
		Boolean actual = this.neuralNetwork.isDifferent(.1d);
		assertEquals(Boolean.FALSE, actual);
	}

	@Test
	public void testIsDifferent() {
		Boolean actual = this.neuralNetwork.isDifferent(.6d);
		assertEquals(Boolean.TRUE, actual);
	}

	@Test
	public void testHasContinue() {
		Boolean actual = this.neuralNetwork.hasContinue(.6d, 9000);
		assertEquals(Boolean.TRUE, actual);
	}

	@Test
	public void testHasNotContinueZeroError() {
		Boolean actual = this.neuralNetwork.hasContinue(.1d, 9000);
		assertEquals(Boolean.FALSE, actual);
	}

	@Test
	public void testHasNotContinueLimitEpochs() {
		Boolean actual = this.neuralNetwork.hasContinue(.6d, 10001);
		assertEquals(Boolean.FALSE, actual);
	}

}