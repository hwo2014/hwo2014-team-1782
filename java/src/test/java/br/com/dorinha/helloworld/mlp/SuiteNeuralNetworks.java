package br.com.dorinha.helloworld.mlp;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.com.dorinha.helloworld.mlp.learning.TestBackPropagation;
import br.com.dorinha.helloworld.mlp.network.TestFunction;
import br.com.dorinha.helloworld.mlp.network.TestNeuralNetwork;
import br.com.dorinha.helloworld.mlp.network.TestUtils;

@RunWith(Suite.class)
@SuiteClasses({
	TestFunction.class,
	TestUtils.class,
	TestBackPropagation.class,
	TestNeuralNetwork.class
})
public class SuiteNeuralNetworks {
}