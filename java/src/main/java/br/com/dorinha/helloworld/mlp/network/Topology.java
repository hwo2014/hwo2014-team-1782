package br.com.dorinha.helloworld.mlp.network;

public class Topology {

	private Integer x;
	private Integer y;
	private Integer u;

	public Topology(Integer x, Integer y, Integer u) {
		super();
		this.x = x;
		this.y = y;
		this.u = u;
	}
	
	public Double[][] createMiddleLayerWeights() {
		return new Double[x][y];
	}
	
	public Double[][] createLastLayerWeights() {
		return new Double[y][u];
	}
}