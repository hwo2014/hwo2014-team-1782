package br.com.dorinha.helloworld.mlp.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

	public static void info(String label, Double[] v) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
		System.out.println("[" + label + "] " + dateFormat.format(new Date()));
		for (int i = 0; i < v.length; i++) {
			System.out.println("[" + i + "] " + v[i]);
		}
	}

}