package br.com.dorinha.helloworld.mlp.learning;

import java.io.Serializable;

public interface LearningAlgorithm extends Serializable {

	public Double[] calculateOutputMiddleLayer(Double[] x, Double[][] w1);

	public Double[] calculateOutputLastLayer(Double[] y, Double[][] w2);

	public Double[] calculateOutputLastLayerError(Double[] d, Double[] u);

	public void refreshLastLayerWeights(Double[] middleLayerOutputs, Double[] e, Double[][] w2);

	public void refreshMiddleLayerWeights(Double[] x, Double[][] w1, Double[] y, Double[][] w2, Double[] e);

	public Double[] classify(Double[] inputs, Double[][] w1, Double[][] w2);

	public Double[] learning(Double[] x, Double[] expect, Double[][] w1,
			Double[][] w2);

}