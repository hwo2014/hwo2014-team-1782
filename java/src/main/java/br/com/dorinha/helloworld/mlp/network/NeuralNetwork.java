package br.com.dorinha.helloworld.mlp.network;

import java.io.Serializable;
import java.util.Random;

import br.com.dorinha.helloworld.mlp.learning.LearningAlgorithm;

public class NeuralNetwork implements Serializable {

	private static final long serialVersionUID = 1L;

	private Double[][] w1;
	private Double[][] w2;
	private LearningAlgorithm learningAlgorithm;
	private Double supportedError;

	public NeuralNetwork(Topology topology, LearningAlgorithm learningAlgorithm, Double supportedError) {
		this.w1 = topology.createMiddleLayerWeights();
		this.w2 = topology.createLastLayerWeights();
		this.learningAlgorithm = learningAlgorithm;
		this.supportedError = supportedError;
		this.init();
	}

	private void init() {
		this.assignRandomWeights(w1);
		this.assignRandomWeights(w2);
	}

	public void assignRandomWeights(Double[][] weights) {
		for (int i = 0; i < weights.length; i++) {
			for (int j = 0; j < weights[i].length; j++) {
				weights[i][j] = new Random().nextDouble() - 0.5;
			}
		}
	}

	public void training(Double[][] inputs, Double[][] expect) {
		Double[] error;
		Double[] beforeErrors = Utils.getNewVector(expect.length);
		Double diff = 0d;
		Integer epoch = 0;
		do {
			for (int i = 0; i < inputs.length; i++) {
				error = this.learningAlgorithm.learning(inputs[i], expect[i], this.w1, this.w2);
				diff = Utils.calculateDiff(error, beforeErrors);
				if (this.isDifferent(diff)) {
					beforeErrors = error;
				}
			}
			epoch++;
		} while (this.hasContinue(diff, epoch));
	}

	public void onlineTraining(Double[] inputs, Double expect) {
		this.learningAlgorithm.learning(inputs, new Double[] { expect }, this.w1, this.w2);
	}

	public Double[] classify(Double[] inputs) {
		return this.learningAlgorithm.classify(inputs, this.w1, this.w2);
	}

	public Boolean hasContinue(Double diff, Integer j) {
		return this.isDifferent(diff) && j.compareTo(10000) < 1;
	}

	public Boolean isDifferent(Double diff) {
		return diff > this.supportedError;
	}

}