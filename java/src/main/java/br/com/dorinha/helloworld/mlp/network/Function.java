package br.com.dorinha.helloworld.mlp.network;

public class Function {

	public static Double calculateSigmoid(Double u) {
		return 1.0 / (1.0 + Math.exp(-u));
	}
	
	public static double calculateSigmoidDerivative(Double y) {
		return y * (1.0 - y);
	}
	
}