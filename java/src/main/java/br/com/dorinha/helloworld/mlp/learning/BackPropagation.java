package br.com.dorinha.helloworld.mlp.learning;

import br.com.dorinha.helloworld.mlp.network.Function;
import br.com.dorinha.helloworld.mlp.network.Utils;

public class BackPropagation implements LearningAlgorithm {

	private static final long serialVersionUID = 1L;

	private Double learningRate = .8d;

	public BackPropagation(Double learningRate) {
		this.learningRate = learningRate;
	}

	@Override
	public Double[] calculateOutputMiddleLayer(Double[] x, Double[][] w1) {
		Double[] linearOutputNeuron = this.calculateOutputNeuron(x, w1);
		return Utils.calcuteSigmoidFunction(linearOutputNeuron);
	}

	@Override
	public Double[] calculateOutputLastLayer(Double[] y, Double[][] w2) {
		return calculateOutputNeuron(y, w2);
	}

	private Double[] calculateOutputNeuron(Double[] inputs, Double[][] weights) {
		Double[] u = Utils.getNewVector(weights[0].length);
		for (int i = 0; i < inputs.length; i++) {
			for (int j = 0; j < weights[i].length; j++) {
				u[j] += inputs[i] * weights[i][j];
			}
		}
		return u;
	}

	@Override
	public Double[] calculateOutputLastLayerError(Double[] d, Double[] u) {
		Double[] e = new Double[d.length];
		for (int i = 0; i < e.length; i++) {
			e[i] = -(d[i] - u[i]);
		}
		return e;
	}

	@Override
	public void refreshLastLayerWeights(Double[] y, Double[] e, Double[][] w2) {
		for (int i = 0; i < w2.length; i++) {
			for (int j = 0; j < e.length; j++) {
				w2[i][j] += (-this.learningRate) * e[j] * y[j];
			}
		}
	}

	@Override
	public void refreshMiddleLayerWeights(Double[] x, Double[][] w1, Double[] y, Double[][] w2, Double[] e) {
		Double[] adjustmentFactors = this.calculateAdjustmentFactors(y, w2, e);
		for (int i = 0; i < w1.length; i++) {
			for (int j = 0; j < w1[i].length; j++) {
				w1[i][j] += (-this.learningRate) * adjustmentFactors[j] * x[i];
			}
		}
	}

	public Double[] calculateAdjustmentFactors(Double[] y, Double[][] w2, Double[] e) {
		Double[] adjustmentFactors = new Double[y.length];
		for (int i = 0; i < y.length; i++) {
			Double backPropagateOutputSignal = 0d;
			for (int j = 0; j < w2[i].length; j++) {
				backPropagateOutputSignal += w2[i][j] * e[j];
			}
			adjustmentFactors[i] = Function.calculateSigmoidDerivative(y[i]) * backPropagateOutputSignal;
		}
		return adjustmentFactors;
	}

	@Override
	public Double[] learning(Double[] x, Double[] expect, Double[][] w1, Double[][] w2) {
		Double[] y = calculateOutputMiddleLayer(x, w1);
		Double[] u = calculateOutputLastLayer(y, w2);
		Double[] e = calculateOutputLastLayerError(expect, u);
		this.feedBack(x, y, e, w1, w2);
		return e;
	}

	@Override
	public Double[] classify(Double[] inputs, Double[][] w1, Double[][] w2) {
		Double[] y = this.calculateOutputMiddleLayer(inputs, w1);
		return this.calculateOutputLastLayer(y, w2);
	}

	private void feedBack(Double[] x, Double[] y, Double[] e, Double[][] w1, Double[][] w2) {
		this.refreshLastLayerWeights(y, e, w2);
		this.refreshMiddleLayerWeights(x, w1, y, w2, e);
	}

}