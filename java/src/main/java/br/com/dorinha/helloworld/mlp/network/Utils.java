package br.com.dorinha.helloworld.mlp.network;

public class Utils {

	public static Double[] getNewVector(Integer size) {
		Double[] vector = new Double[size];
		for (int i = 0; i < vector.length; i++) {
			vector[i] = 0d;
		}
		return vector;
	}

	public static Double[] calcuteSigmoidFunction(Double[] vector) {
		for (int i = 0; i < vector.length; i++) {
			vector[i] = Function.calculateSigmoid(vector[i]);
		}
		return vector;
	}

	public static Double calculateDiff(Double[] u, Double[] v) {
		Double diff = 0d;
		for (int i = 0; i < u.length; i++) {
			diff += Math.pow(u[i] - v[i], 2);
		}
		return diff;
	}

	public static Double calculateDiff(Double u, Double v) {
		return Math.pow(u - v, 2);
	}
}