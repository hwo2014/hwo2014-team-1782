package br.com.dorinha.helloworldopen.processor;

import br.com.dorinha.helloworldopen.domain.Car;
import br.com.dorinha.helloworldopen.domain.Game;

import com.google.gson.JsonObject;

public class FinishMessageProcessor implements MessageTypeProcessor {

	@Override
	public void process(Game game, JsonObject json, ServerMessageType type) {
		String color = json.get("data").getAsJsonObject().get("color").getAsString();
		Car car = game.getCar(color);
		car.finish();
	}
}
