package br.com.dorinha.helloworldopen;

import static br.com.dorinha.helloworldopen.utils.JsonObjectBuilder.json;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.dorinha.helloworldopen.domain.Game;
import br.com.dorinha.helloworldopen.domain.MessageSender;
import br.com.dorinha.helloworldopen.domain.Shutdownable;
import br.com.dorinha.helloworldopen.processor.BotMessageType;
import br.com.dorinha.helloworldopen.processor.MessageProcessor;
import br.com.dorinha.helloworldopen.utils.JsonObjectBuilder;

import com.google.gson.JsonObject;

public class GameClient implements MessageSender, Runnable, Shutdownable {

	private static final Logger logger = LoggerFactory.getLogger(GameClient.class);
	private static final int NUMBER_OF_WORKERS = 2;

	private final Game game;
	private final ExecutorService executor;
	private final MessageProcessor processor;
	private final Thread messageReader = new Thread(this, "message-reader");

	private Socket socket;
	private PrintWriter writer;
	private BufferedReader reader;

	public GameClient() {
		this.game = new Game(this, this);
		this.processor = new MessageProcessor(game);
		this.executor = Executors.newFixedThreadPool(NUMBER_OF_WORKERS);
	}

	public void connect(String host, int port) throws UnknownHostException, IOException {
		socket = new Socket(host, port);
		writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
		reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		logger.info("Connected...");
		
		messageReader.start();
	}

	public void join(String botName, String botKey) {
		logger.info("Joining {}/{}", botName, botKey);
		send(json().with("msgType", BotMessageType.join.name()).with("data",
				json().with("name", botName).with("key", botKey)));
	}

	public void send(JsonObjectBuilder json) {
		send(json.toJson());
	}

	public void send(JsonObject json) {
		send(json.toString());
	}

	public void send(String message) {
		writer.println(message);
		writer.flush();
	}

	@Override
	public void run() {
		try {
			while (true) {
				String message = reader.readLine();
				if (message == null) {
					break;
				} else {
					executor.execute(new Message(message));
				}
			}
		} catch (IOException e) {
			logger.error("IOException while reading messages from server.", e);
		}
	}

	public void waitForFinish() throws InterruptedException {
		messageReader.join();
	}

	private final class Message implements Runnable {

		private final String message;

		public Message(String message) {
			super();
			this.message = message;
		}

		@Override
		public void run() {
			processor.process(message);
		}
	}

	@Override
	public void shutdown() {
		try {
			this.socket.close();
			this.executor.shutdown();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
