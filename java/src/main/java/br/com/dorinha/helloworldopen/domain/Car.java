package br.com.dorinha.helloworldopen.domain;

import java.time.temporal.ChronoUnit;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Car {

	private static final Logger logger = LoggerFactory.getLogger(Car.class);
	private final String color;
	private final Game game;
	private double length;
	private double guideFlagPosition;
	private double width;
	private double angle;
	private double velocity;
	private int ranking;
	private Position position;
	private Position lastPosition;

	public Car(String color, Game game) {
		this.color = color;
		this.game = game;
	}

	public String getColor() {
		return color;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public void setGuideFlagPosition(double guideFlagPosition) {
		this.guideFlagPosition = guideFlagPosition;
	}

	public double getLength() {
		return length;
	}

	public double getGuideFlagPosition() {
		return guideFlagPosition;
	}

	public double getWidth() {
		return width;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public double getAngle() {
		return angle;
	}

	public int getLap() {
		if (position == null)
			return 0;
		return position.getLap();
	}

	public void setPiecePosition(Piece piece, Lane startLane, Lane endLane, double inPieceDistance, int lap) {
		this.lastPosition = this.position;
		this.position = new Position(piece, startLane, endLane, inPieceDistance, lap);
		this.velocity = calculateVelocity(lastPosition, position);
	}

	protected double calculateVelocity(Position initialPosition, Position endPosition) {
		if (initialPosition == null || endPosition == null) {
			return 0;
		}

		double duration = ChronoUnit.MILLIS.between(initialPosition.getTimestamp(), endPosition.getTimestamp());
		double distance = 0;
		if (initialPosition.getPiece().getIndex() == endPosition.getPiece().getIndex()) {
			distance = calculateDistanceIntTheSamePiece(initialPosition, endPosition);
		} else {
			distance = calculateDistanceOfStartAndEndPieces(initialPosition, endPosition);
			distance = incrementDistanceBetweenPieces(initialPosition, endPosition, distance);
		}
		if (distance == 0.0)
			return 0.0;
		return distance / duration;
	}

	private double incrementDistanceBetweenPieces(Position initialPosition, Position endPosition, double distance) {
		if (endPosition.getPiece().getIndex() - initialPosition.getPiece().getIndex() > 1) {
			List<Piece> pieces = game.race().track()
					.getPiecesBetweenIndexes(initialPosition.getPiece().getIndex(), endPosition.getPiece().getIndex());
			for (Piece piece : pieces) {
				distance += piece.getLength();
			}
		}
		return distance;
	}

	private double calculateDistanceOfStartAndEndPieces(Position initialPosition, Position endPosition) {
		return initialPosition.getInPieceRemainingDistance() + endPosition.getInPieceDistance();
	}

	private double calculateDistanceIntTheSamePiece(Position initialPosition, Position endPosition) {
		return endPosition.getInPieceDistance() - initialPosition.getInPieceDistance();
	}

	public Position getPosition() {
		return position;
	}

	public double getVelocity() {
		return velocity;
	}

	public double getCurrentPieceAngle() {
		if (position == null)
			return 0;
		return position.getPiece().getAngle();
	}

	public void crash() {
		if (game.getMyCar() == this) {
			logger.info("Ops, I crashed!");
			game.knowledgeBase().notifyStretchFail();
		}
	}

	public void lapFinished(long lapTime) {
		if (game.getMyCar() == this) {
			Lap lap = game.lap();
			lap.finish(lapTime);
			game.knowledgeBase().notifyLapResult(lap.getLapIndex(), game.getLastLapResult());
		}
	}

	public void setRanking(int ranking) {
		this.ranking = ranking;
	}

	public int getRanking() {
		return ranking;
	}

	public PieceAngleForecasts getPieceAngleForecasts() {
		if (position == null || lastPosition == null) {
			return new PieceAngleForecasts(0, 0, 0, 0, 0);
		}

		final long timeFrame = ChronoUnit.MILLIS.between(position.getTimestamp(), lastPosition.getTimestamp());
		final Track track = game.getTrack();

		return new PieceAngleForecasts(track.forecastAngle(velocity, timeFrame, 1, position.getPiece().getIndex(),
				position.getInPieceRemainingDistance()), track.forecastAngle(velocity, timeFrame, 2, position
				.getPiece().getIndex(), position.getInPieceRemainingDistance()), track.forecastAngle(velocity,
				timeFrame, 3, position.getPiece().getIndex(), position.getInPieceRemainingDistance()),
				track.forecastAngle(velocity, timeFrame, 5, position.getPiece().getIndex(),
						position.getInPieceRemainingDistance()), track.forecastAngle(velocity, timeFrame, 8, position
						.getPiece().getIndex(), position.getInPieceRemainingDistance()));
	}

	public SwitchLanePotentials getSwitchLanePotentials() {
		return new SwitchLanePotentials(0, 0, 0);
	}

	public void finish() {
	}
}
