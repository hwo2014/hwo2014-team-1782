package br.com.dorinha.helloworldopen.processor;

public enum ServerMessageType {

	YOURCAR, GAMEINIT, GAMESTART, CARPOSITIONS, GAMEEND, TOURNAMENTEND, CRASH, SPAWN, LAPFINISHED, DNF, FINISH;
}
