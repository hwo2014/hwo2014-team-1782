package br.com.dorinha.helloworldopen.processor;

import br.com.dorinha.helloworldopen.domain.Game;

import com.google.gson.JsonObject;

public class TournamentEndMessageProcessor implements MessageTypeProcessor {

	@Override
	public void process(Game game, JsonObject json, ServerMessageType type) {
		game.shutdown();
	}
}
