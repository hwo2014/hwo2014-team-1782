package br.com.dorinha.helloworldopen.domain;

public class SwitchLanePotentials {

    protected final double leftLanePotential;
    protected final double stayLanePotential;
    protected final double rightLanePotential;

    public SwitchLanePotentials(double leftLanePotential, double stayLanePotential, double rightLanePotential) {
        this.leftLanePotential = leftLanePotential;
        this.stayLanePotential = stayLanePotential;
        this.rightLanePotential = rightLanePotential;
    }
}
