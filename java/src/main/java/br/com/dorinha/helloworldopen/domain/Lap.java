package br.com.dorinha.helloworldopen.domain;

public class Lap {

	private final int lap;
	private final Game game;
	private long lapTime;

	public Lap(int lap, Game game) {
		this.lap = lap;
		this.game = game;
	}

	public void finish(long lapTime) {
		this.lapTime = lapTime;
		game.newLap(lap + 1);
	}

	public int getLapIndex() {
		return lap;
	}

	public long getLapTime() {
		return lapTime;
	}
}
