package br.com.dorinha.helloworldopen;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

	private static final Logger logger = LoggerFactory.getLogger(Main.class);

	public static void main(String... args) throws IOException, InterruptedException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		logger.info("Connecting to {}:{} as {}/{}", host, port, botName, botKey);

		GameClient client = new GameClient();
		client.connect(host, port);
		client.join(botName, botKey);
		client.waitForFinish();
	}
}
