package br.com.dorinha.helloworldopen.domain;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.dorinha.helloworldopen.Classifier;
import br.com.dorinha.helloworldopen.MLPClassifier;

public class KnowledgeBase {

	private static final Logger logger = LoggerFactory.getLogger(KnowledgeBase.class);
	private static final String defaultDensityFactorKnowledgeStore = "knowledge-store.density-factor.data";

	private final Classifier densityFactorClassifier;
	private final List<Knowledge> base = new ArrayList<>();
	private final Map<Integer, List<Knowledge>> laps = new HashMap<>();
	private final String densityFactorKnowledgeStore;

	public KnowledgeBase() {
		this(defaultDensityFactorKnowledgeStore);
	}

	public KnowledgeBase(String knowledgeStore) {
		this.densityFactorKnowledgeStore = knowledgeStore;
		File store = new File(knowledgeStore);
		if (store.exists() && store.isFile() && store.canRead()) {
			logger.info("Loading density factor knowledge base from : {}", knowledgeStore);
			try (FileInputStream fileIn = new FileInputStream(store);
					ObjectInputStream objectIn = new ObjectInputStream(fileIn)) {
				this.densityFactorClassifier = (Classifier) objectIn.readObject();
				logger.info("Density factor knowledge base loaded.", knowledgeStore);
			} catch (IOException e) {
				throw new RuntimeException(e);
			} catch (ClassNotFoundException e) {
				throw new RuntimeException(e);
			}
		} else {
			densityFactorClassifier = new MLPClassifier(8);
			densityFactorClassifier.on(0d, 1d, 0d, 0d, 0d, 0d, 0d, 0d).expect(1d);
		}
	}

	public void createKnowledge(int lap, double angle, double velocity, double currentPieceAngle,
			PieceAngleForecasts pieceAngleForecasts, SwitchLanePotentials switchLanePotentials, double densityFactor,
			SwitchLaneDecision lane) {
		Fact fact = new Fact(angle, velocity, currentPieceAngle, pieceAngleForecasts.nextFirstTickForecastAngle,
				pieceAngleForecasts.nextSecondTickForecastAngle, pieceAngleForecasts.nextThirdTickForecastAngle,
				pieceAngleForecasts.nextFifthTickForecastAngle, pieceAngleForecasts.nextEighthForecastAngle,
				switchLanePotentials.leftLanePotential, switchLanePotentials.stayLanePotential,
				switchLanePotentials.rightLanePotential);
		Decision decision = new Decision(densityFactor, lane);
		Knowledge knowledge = new Knowledge(fact, decision);

		this.base.add(knowledge);

		if (!this.laps.containsKey(lap)) {
			this.laps.put(lap, new ArrayList<Knowledge>());
		}
		this.laps.get(lap).add(knowledge);
	}

	public void notifyStretchFail() {
		for (int i = base.size() - 1; i > base.size() - 11; i--) {
			base.get(i).notifyStretchFail();
		}
	}

	public void notifyLapResult(int lap, double lastLapResult) {
		for (Knowledge k : new ArrayList<>(this.laps.get(lap))) {
			k.notifyLapResult(lastLapResult);
		}
		learnDensityFactor(lap);
	}

	private void learnDensityFactor(int lap) {
		for (Knowledge knowledge : laps.get(lap)) {
			if (knowledge.success()) {
				try {
					densityFactorClassifier.on(knowledge.fact.angle, knowledge.fact.velocity,
							knowledge.fact.currentPieceAngle, knowledge.fact.nextFirstTickForecastAngle,
							knowledge.fact.nextSecondTickForecastAngle, knowledge.fact.nextThirdTickForecastAngle,
							knowledge.fact.nextFifthTickForecastAngle, knowledge.fact.nextEighthForecastAngle).expect(
							knowledge.decision.densityFactor);
				} catch (Exception e) {
					logger.error("Error classifying instance.", e);
				}
			}
		}
	}

	@SafeVarargs
	public static <T> List<T> toList(T... values) {
		List<T> result = new ArrayList<>();
		for (T value : values) {
			result.add(value);
		}
		return result;
	}

	public void persist() {
		logger.info("Persisting density factor knowledge base to : {}", densityFactorKnowledgeStore);
		try (FileOutputStream fileOut = new FileOutputStream(densityFactorKnowledgeStore);
				ObjectOutputStream objectOut = new ObjectOutputStream(fileOut)) {
			objectOut.writeObject(this.densityFactorClassifier);
			logger.info("Density factor knowledge base persisted.", densityFactorKnowledgeStore);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public double densityFactor(double angle, double velocity, double currentPieceAngle,
			PieceAngleForecasts pieceAngleForecasts) {
		try {
			return densityFactorClassifier.classify(angle, velocity, currentPieceAngle,
					pieceAngleForecasts.nextFirstTickForecastAngle, pieceAngleForecasts.nextSecondTickForecastAngle,
					pieceAngleForecasts.nextThirdTickForecastAngle, pieceAngleForecasts.nextFifthTickForecastAngle,
					pieceAngleForecasts.nextEighthForecastAngle);
		} catch (Exception e) {
			logger.error("Error classifying instance.", e);
			return 1.0;
		}
	}
}

class Fact {

	final double angle;
	final double velocity;
	final double currentPieceAngle;
	final double nextFirstTickForecastAngle;
	final double nextSecondTickForecastAngle;
	final double nextThirdTickForecastAngle;
	final double nextFifthTickForecastAngle;
	final double nextEighthForecastAngle;
	final double leftLanePotential;
	final double stayLanePotential;
	final double rightLanePotential;

	public Fact(double angle, double velocity, double currentPieceAngle, double nextFirstTickForecastAngle,
			double nextSecondTickForecastAngle, double nextThirdTickForecastAngle, double nextFifthTickForecastAngle,
			double nextEighthForecastAngle, double leftLanePotential, double stayLanePotential,
			double rightLanePotential) {
		this.angle = angle;
		this.velocity = velocity;
		this.currentPieceAngle = currentPieceAngle;
		this.nextFirstTickForecastAngle = nextFirstTickForecastAngle;
		this.nextSecondTickForecastAngle = nextSecondTickForecastAngle;
		this.nextThirdTickForecastAngle = nextThirdTickForecastAngle;
		this.nextFifthTickForecastAngle = nextFifthTickForecastAngle;
		this.nextEighthForecastAngle = nextEighthForecastAngle;
		this.leftLanePotential = leftLanePotential;
		this.stayLanePotential = stayLanePotential;
		this.rightLanePotential = rightLanePotential;
	}
}

class Decision {

	final double densityFactor;
	final SwitchLaneDecision lane;

	public Decision(double densityFactor, SwitchLaneDecision lane) {
		this.densityFactor = densityFactor;
		this.lane = lane;
	}
}

class Knowledge {

	final Fact fact;
	final Decision decision;
	double successFactor = 0.0;

	public Knowledge(Fact fact, Decision decision) {
		this.fact = fact;
		this.decision = decision;
	}

	public boolean success() {
		return successFactor >= 0.3;
	}

	public void notifyLapResult(double lastLapResult) {
		if (successFactor >= 0.0) {
			successFactor = lastLapResult;
		}
	}

	public void notifyStretchFail() {
		successFactor = -1.0;
	}
}
