package br.com.dorinha.helloworldopen.processor;

import br.com.dorinha.helloworldopen.domain.Game;

import com.google.gson.JsonObject;

public interface MessageTypeProcessor {

	void process(Game game, JsonObject json, ServerMessageType type);
}
