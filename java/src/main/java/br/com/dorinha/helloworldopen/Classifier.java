package br.com.dorinha.helloworldopen;

import java.io.Serializable;

public interface Classifier extends Serializable {

	public double classify(Double... input);

	TrainingInstance on(Double... input);

	public static interface TrainingInstance {

		public void expect(Double expect);
	}
}
