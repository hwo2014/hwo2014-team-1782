package br.com.dorinha.helloworldopen.domain;

import static br.com.dorinha.helloworldopen.utils.JsonObjectBuilder.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.dorinha.helloworldopen.processor.BotMessageType;
import br.com.dorinha.helloworldopen.utils.JsonObjectBuilder;

import com.google.gson.JsonObject;

public class Game {

	private static final Logger logger = LoggerFactory.getLogger(Game.class);

	private Car myCar;
	private Race race;

	private final MessageSender sender;
	private final Shutdownable shutdownable;
	private final Map<String, Car> cars = new HashMap<>();
	private final CountDownLatch waitConfigure = new CountDownLatch(2);
	private final List<Lap> laps = new ArrayList<>();
	private final KnowledgeBase knowledgeBase = new KnowledgeBase();

	public Game(MessageSender sender, Shutdownable shutdownable) {
		this.sender = sender;
		this.shutdownable = shutdownable;
	}

	public Game() {
		this.sender = new MessageSender() {

			@Override
			public void send(String message) {
			}

			@Override
			public void send(JsonObject json) {
			}

			@Override
			public void send(JsonObjectBuilder json) {
			}
		};
		this.shutdownable = new Shutdownable() {

			@Override
			public void shutdown() {
			}
		};
	}

	public void configureMyCar(Car car) {
		this.myCar = car;
		cars.put(car.getColor(), car);
	}

	public Car getMyCar() {
		return myCar;
	}

	public Race race() {
		if (this.race == null) {
			this.race = new Race();
		}
		return race;
	}

	public Car getCar(String color) {
		Car car = cars.get(color);
		if (car == null) {
			car = new Car(color, this);
			cars.put(color, car);
		}
		return car;
	}

	public void start(long tick) {
		start();
		throttle(0.75, 1.0, tick);
	}

	public void start() {
		logger.info("Game start...");
		this.lap();
		this.waitConfigure.countDown();
	}

	public void init() {
		logger.info("Game init...");
		this.waitConfigure.countDown();
	}

	public void waitConfigure() {
		try {
			this.waitConfigure.await();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	public Track getTrack() {
		return race().getTrack();
	}

	public void throttle(double value, double densityFactor, long tick) {
		double throttle = value * densityFactor;
		sender.send(json().with("msgType", BotMessageType.throttle.name()).with("data", throttle)
				.with("gameTick", tick));
		logger.info("throttle : {} - density factor : {}", throttle, densityFactor);
		registerAction(densityFactor, SwitchLaneDecision.Stay);
	}

	public void registerAction(double densityFactor, SwitchLaneDecision lane) {
		knowledgeBase.createKnowledge(getCurrentLapIndex(), myCar.getAngle(), myCar.getVelocity(),
				myCar.getCurrentPieceAngle(), myCar.getPieceAngleForecasts(), myCar.getSwitchLanePotentials(),
				densityFactor, lane);
	}

	private int getCurrentLapIndex() {
		return laps.get(laps.size() - 1).getLapIndex();
	}

	public void tick(long tick) {
		logger.info("tick : {}, angle : {}, velocity : {}, pieceAngle : {}", tick, myCar.getAngle(),
				myCar.getVelocity(), myCar.getCurrentPieceAngle());

		double densityFactor = knowledgeBase.densityFactor(myCar.getAngle(), myCar.getVelocity(),
				myCar.getCurrentPieceAngle(), myCar.getPieceAngleForecasts());

		double angleInRadians = Math.toRadians(myCar.getCurrentPieceAngle());
		double throttle = Math.abs(Math.cos(angleInRadians));
		if (throttle < 0.1) {
			throttle = 0.1;
		}

		throttle(throttle, densityFactor, tick);
	}

	public Lap lap() {
		if (this.laps.isEmpty()) {
			newLap(0);
		}
		return this.laps.get(this.laps.size() - 1);
	}

	public void newLap(int lap) {
		this.laps.add(new Lap(lap, this));
	}

	public KnowledgeBase knowledgeBase() {
		return knowledgeBase;
	}

	public double getLastLapResult() {
		if (laps.size() < 2) {
			return 0;
		} else {
			Lap last = laps.get(laps.size() - 1);
			Lap lastButOne = laps.get(laps.size() - 2);
			return (last.getLapTime() - lastButOne.getLapTime()) / Math.max(last.getLapTime(), lastButOne.getLapTime());
		}
	}

	public void shutdown() {
		knowledgeBase.persist();
		shutdownable.shutdown();
	}
}
