package br.com.dorinha.helloworldopen.processor;

import br.com.dorinha.helloworldopen.domain.Car;
import br.com.dorinha.helloworldopen.domain.Game;

import com.google.gson.JsonObject;

public class YourCarMessageProcessor implements MessageTypeProcessor {

	@Override
	public void process(Game game, JsonObject json, ServerMessageType type) {
		game.configureMyCar(new Car(json.get("data").getAsJsonObject().get("color").getAsString(), game));
	}
}
