package br.com.dorinha.helloworldopen;

import br.com.dorinha.helloworld.mlp.learning.BackPropagation;
import br.com.dorinha.helloworld.mlp.learning.LearningAlgorithm;
import br.com.dorinha.helloworld.mlp.network.NeuralNetwork;
import br.com.dorinha.helloworld.mlp.network.Topology;

public class MLPClassifier implements Classifier {

	private static final long serialVersionUID = 1L;

	private static final double LEARNING_RATE = .8;
	private static final double SUPPORTED_ERROR = .1;

	private final NeuralNetwork neuralNetwork;
	private boolean firstTraining = true;

	public MLPClassifier(int numberOfInputAttributes) {
		super();
		Topology topology = new Topology(numberOfInputAttributes, (numberOfInputAttributes) / 2 + 1, 1);
		LearningAlgorithm learningAlgorithm = new BackPropagation(LEARNING_RATE);

		neuralNetwork = new NeuralNetwork(topology, learningAlgorithm, SUPPORTED_ERROR);
	}

	@Override
	public TrainingInstance on(Double... input) {
		return new TrainingInstanceImpl(input);
	}

	@Override
	public double classify(Double... input) {
		return neuralNetwork.classify(input)[0];
	}

	public final class TrainingInstanceImpl implements TrainingInstance {

		private final Double[] input;

		public TrainingInstanceImpl(Double[] input) {
			this.input = input;
		}

		public void expect(Double expect) {
			if (firstTraining) {
				firstTraining = false;
				neuralNetwork.training(new Double[][] { input }, new Double[][] { { expect } });
			} else {
				neuralNetwork.onlineTraining(input, expect);
			}
		}
	}
}
