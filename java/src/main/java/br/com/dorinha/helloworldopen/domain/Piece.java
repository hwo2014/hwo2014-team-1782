package br.com.dorinha.helloworldopen.domain;

public class Piece {

    private double length;
    private boolean hasSwitch;
    private double radius;
    private double angle;
    private int index;

    public void setIndex(int index) {
        this.index = index;
    }

    public Piece setLength(double length) {
        this.length = length;
        return this;
    }

    public void setHasSwitch(boolean value) {
        this.hasSwitch = value;
    }

    public Piece setRadius(double radius) {
        this.radius = radius;
        return this;
    }

    public Piece setAngle(double angle) {
        this.angle = angle;
        return this;
    }

    public double getLength() {
        if (length == 0) {
            return getArcLength();
        }
        return length;
    }

    protected double getArcLength() {
        return 2.0 * angle * Math.PI * radius / 360.0;
    }

    public boolean hasSwitch() {
        return hasSwitch;
    }

    public double getRadius() {
        return radius;
    }

    public double getAngle() {
        return angle;
    }

    public int getIndex() {
        return index;
    }
}
