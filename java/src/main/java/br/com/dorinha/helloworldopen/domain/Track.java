package br.com.dorinha.helloworldopen.domain;

import java.util.ArrayList;
import java.util.List;

public class Track {

    private String id;
    private StartingPoint startingPoint;
    private final List<Piece> pieces = new ArrayList<>();
    private final List<Lane> lanes = new ArrayList<>();

    public void setId(String id) {
        this.id = id;
    }

    public StartingPoint startingPoint() {
        this.startingPoint = new StartingPoint();
        return startingPoint;
    }

    public Piece createPiece() {
        Piece piece = new Piece();
        pieces.add(piece);
        piece.setIndex(pieces.indexOf(piece));
        return piece;
    }

    public Lane createLane() {
        Lane lane = new Lane();
        lanes.add(lane);
        return lane;
    }

    public String getId() {
        return id;
    }

    public List<Piece> getPieces() {
        return pieces;
    }

    public List<Lane> getLanes() {
        return lanes;
    }

    public StartingPoint getStartingPoint() {
        return startingPoint;
    }

    public Piece getPieceOfIndex(int index) {
        if (pieces.size() < index + 1) {
            throw new RuntimeException("Piece of index " + index + " does not exist - current pieces size are "
                    + pieces.size());
        }
        return pieces.get(index);
    }

    public Lane getLaneOfIndex(int index) {
        if (lanes.size() < index + 1) {
            throw new RuntimeException("Lane of index " + index + " does not exist - current lanes size are "
                    + lanes.size());
        }
        return lanes.get(index);
    }

    public List<Piece> getPiecesBetweenIndexes(int start, int end) {
        return this.pieces.subList(start + 1, end);
    }

    public double forecastAngle(double velocity, long timeFrame, int numberOfFrames, int pieceIndex, double inPieceRemainingDistance) {
        double distance = velocity * timeFrame * numberOfFrames;
        distance -= inPieceRemainingDistance;

        while (distance > 0) {
            pieceIndex++;
            if (pieceIndex == pieces.size()) {
                pieceIndex = 0;
            }
            distance -= getPieceOfIndex(pieceIndex).getArcLength();
        }

        return getPieceOfIndex(pieceIndex).getAngle();
    }
}
