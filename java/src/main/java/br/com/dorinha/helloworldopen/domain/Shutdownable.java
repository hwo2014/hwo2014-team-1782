package br.com.dorinha.helloworldopen.domain;

public interface Shutdownable {

	void shutdown();
}
