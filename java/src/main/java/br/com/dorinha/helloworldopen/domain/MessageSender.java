package br.com.dorinha.helloworldopen.domain;

import br.com.dorinha.helloworldopen.utils.JsonObjectBuilder;

import com.google.gson.JsonObject;

public interface MessageSender {

	void send(JsonObjectBuilder json);

	void send(JsonObject json);

	void send(String message);
}
