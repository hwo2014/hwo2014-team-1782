package br.com.dorinha.helloworldopen.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;

public class JsonObjectBuilder {

	private JsonObject root = new JsonObject();
	private JsonObject ref = root;

	public static JsonObjectBuilder json() {
		return new JsonObjectBuilder();
	}

	public JsonObjectBuilder with(String name, String value) {
		if (value == null) {
			ref.add(name, JsonNull.INSTANCE);
		} else {
			ref.addProperty(name, value);
		}
		return this;
	}

	public JsonObjectBuilder with(String name, Long value) {
		if (value == null) {
			ref.add(name, JsonNull.INSTANCE);
		} else {
			ref.addProperty(name, value);
		}
		return this;
	}

	public JsonObjectBuilder with(String name, int value) {
		ref.addProperty(name, value);
		return this;
	}

	public JsonObjectBuilder with(String name, Integer value) {
		if (value == null) {
			ref.add(name, JsonNull.INSTANCE);
		} else {
			ref.addProperty(name, value);
		}
		return this;
	}

	public JsonObjectBuilder with(String name, boolean value) {
		ref.addProperty(name, value);
		return this;
	}

	public JsonObjectBuilder with(String name, Boolean value) {
		ref.addProperty(name, value);
		return this;
	}

	public JsonObject toJson() {
		return root;
	}

	@Override
	public String toString() {
		return toJson().toString();
	}

	public JsonObjectBuilder with(String name, double value) {
		ref.addProperty(name, value);
		return this;
	}

	public JsonObjectBuilder with(String name, long value) {
		ref.addProperty(name, value);
		return this;
	}

	public JsonObjectBuilder with(String name, JsonElement jsonElement) {
		ref.add(name, jsonElement);
		return this;
	}

	public JsonObjectBuilder with(String name, JsonObjectBuilder json) {
		ref.add(name, json.toJson());
		return this;
	}
}
