package br.com.dorinha.helloworldopen.domain;

public class PieceAngleForecasts {

    protected final double nextFirstTickForecastAngle;
    protected final double nextSecondTickForecastAngle;
    protected final double nextThirdTickForecastAngle;
    protected final double nextFifthTickForecastAngle;
    protected final double nextEighthForecastAngle;

    public PieceAngleForecasts(double nextFirstTickForecastAngle, double nextSecondTickForecastAngle, double nextThirdTickForecastAngle, double nextFifthTickForecastAngle,
                               double nextEighthForecastAngle) {
        this.nextFirstTickForecastAngle = nextFirstTickForecastAngle;
        this.nextSecondTickForecastAngle = nextSecondTickForecastAngle;
        this.nextThirdTickForecastAngle = nextThirdTickForecastAngle;
        this.nextFifthTickForecastAngle = nextFifthTickForecastAngle;
        this.nextEighthForecastAngle = nextEighthForecastAngle;
    }
}
