package br.com.dorinha.helloworldopen.processor;

import java.util.Iterator;

import br.com.dorinha.helloworldopen.domain.Car;
import br.com.dorinha.helloworldopen.domain.Game;
import br.com.dorinha.helloworldopen.domain.Lane;
import br.com.dorinha.helloworldopen.domain.Piece;
import br.com.dorinha.helloworldopen.domain.Race;
import br.com.dorinha.helloworldopen.domain.StartingPoint;
import br.com.dorinha.helloworldopen.domain.Track;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class GameInitMessageProcessor implements MessageTypeProcessor {

	@Override
	public void process(Game game, JsonObject json, ServerMessageType type) {
		Race race = game.race();
		process(game, race, json.get("data").getAsJsonObject().get("race").getAsJsonObject());
		game.init();
	}

	private void process(Game game, Race race, JsonObject json) {
		Track track = race.track();
		processTrack(track, json.get("track").getAsJsonObject());

		JsonArray cars = json.get("cars").getAsJsonArray();
		Iterator<JsonElement> iterator = cars.iterator();
		while (iterator.hasNext()) {
			processCar(game, iterator.next().getAsJsonObject());
		}

		processSession(race, json.get("raceSession").getAsJsonObject());
	}

	private void processSession(Race race, JsonObject json) {
		race.setLaps(json.get("laps").getAsInt());
		race.setMaxLapTimeMs(json.get("maxLapTimeMs").getAsLong());
		race.setQuickRace(json.get("quickRace").getAsBoolean());
	}

	private void processCar(Game game, JsonObject json) {
		Car car = game.getCar(json.get("id").getAsJsonObject().get("color").getAsString());
		processDimensions(car, json.get("dimensions").getAsJsonObject());
	}

	private void processDimensions(Car car, JsonObject json) {
		car.setLength(json.get("length").getAsDouble());
		car.setWidth(json.get("width").getAsDouble());
		car.setGuideFlagPosition(json.get("guideFlagPosition").getAsDouble());
	}

	private void processTrack(Track track, JsonObject json) {
		track.setId(json.get("id").getAsString());

		JsonArray pieces = json.get("pieces").getAsJsonArray();
		Iterator<JsonElement> iterator = pieces.iterator();
		while (iterator.hasNext()) {
			processPiece(track, iterator.next().getAsJsonObject());
		}

		JsonArray lanes = json.get("lanes").getAsJsonArray();
		iterator = lanes.iterator();
		while (iterator.hasNext()) {
			processLane(track, iterator.next().getAsJsonObject());
		}

		processStartPoint(track.startingPoint(), json.get("startingPoint").getAsJsonObject());
	}

	private void processStartPoint(StartingPoint startingPoint, JsonObject json) {
		startingPoint.setX(json.get("position").getAsJsonObject().get("x").getAsDouble());
		startingPoint.setY(json.get("position").getAsJsonObject().get("y").getAsDouble());
		startingPoint.setAngle(json.get("angle").getAsDouble());
	}

	private void processLane(Track track, JsonObject json) {
		Lane lane = track.createLane();

		lane.setDistanceFromCenter(json.get("distanceFromCenter").getAsDouble());
		lane.setIndex(json.get("index").getAsInt());
	}

	private void processPiece(Track track, JsonObject json) {
		Piece piece = track.createPiece();

		if (json.has("length")) {
			piece.setLength(json.get("length").getAsDouble());
		}

		if (json.has("switch")) {
			piece.setHasSwitch(json.get("switch").getAsBoolean());
		}

		if (json.has("radius")) {
			piece.setRadius(json.get("radius").getAsDouble());
		}

		if (json.has("angle")) {
			piece.setAngle(json.get("angle").getAsDouble());
		}
	}
}
