package br.com.dorinha.helloworldopen.domain;

public enum SwitchLaneDecision {
    Left, Right, Stay;
}
