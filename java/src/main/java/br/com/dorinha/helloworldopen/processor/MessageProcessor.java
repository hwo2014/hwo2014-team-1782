package br.com.dorinha.helloworldopen.processor;

import br.com.dorinha.helloworldopen.domain.Game;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class MessageProcessor {

    private static final Logger logger = LoggerFactory.getLogger(MessageProcessor.class);
    private final Map<ServerMessageType, MessageTypeProcessor> processors = new HashMap<>();
    private final JsonParser parser;
    private final Game game;

    public MessageProcessor(Game game) {
        this.game = game;
        this.parser = new JsonParser();

        this.processors.put(ServerMessageType.YOURCAR, new YourCarMessageProcessor());
        this.processors.put(ServerMessageType.GAMEINIT, new GameInitMessageProcessor());
        this.processors.put(ServerMessageType.GAMESTART, new GameStartMessageProcessor());
        this.processors.put(ServerMessageType.CARPOSITIONS, new CarPositionsMessageProcessor());
        this.processors.put(ServerMessageType.CRASH, new CrashMessageProcessor());
        this.processors.put(ServerMessageType.LAPFINISHED, new LapFinishedMessageProcessor());
        this.processors.put(ServerMessageType.FINISH, new FinishMessageProcessor());
        this.processors.put(ServerMessageType.GAMEEND, new GameEndMessageProcessor());
        this.processors.put(ServerMessageType.TOURNAMENTEND, new TournamentEndMessageProcessor());
    }

    public void process(String message) {
        logger.info(message);

        final JsonObject json = this.parser.parse(message).getAsJsonObject();
        final String type = json.get("msgType").getAsString().toUpperCase();

        try {
            process(json, ServerMessageType.valueOf(type));
        } catch (IllegalArgumentException e) {
        }
    }

    private void process(JsonObject json, ServerMessageType type) {
        MessageTypeProcessor processor = this.processors.get(type);
        if (processor != null) {
            processor.process(game, json, type);
        } else {
            logger.warn("Message of type {} not yet implemented.");
        }
    }
}
