package br.com.dorinha.helloworldopen.domain;

import java.time.LocalDateTime;

public class Position {

	private final Piece piece;
	private final Lane startLane;
	private final Lane endLane;
	private final double inPieceDistance;
	private final int lap;
	private final LocalDateTime timestamp;

	public Position(Piece piece, Lane startLane, Lane endLane, double inPieceDistance, int lap) {
		this.piece = piece;
		this.startLane = startLane;
		this.endLane = endLane;
		this.inPieceDistance = inPieceDistance;
		this.lap = lap;
		this.timestamp = LocalDateTime.now();
	}

	public int getLap() {
		return lap;
	}

	public Piece getPiece() {
		return piece;
	}

	public Lane getStartLane() {
		return startLane;
	}

	public Lane getEndLane() {
		return endLane;
	}

	public double getInPieceDistance() {
		return inPieceDistance;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public double getInPieceRemainingDistance() {
		return this.getPiece().getLength() - getInPieceDistance();
	}
}
