package br.com.dorinha.helloworldopen.processor;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.dorinha.helloworldopen.domain.Car;
import br.com.dorinha.helloworldopen.domain.Game;
import br.com.dorinha.helloworldopen.domain.Lane;
import br.com.dorinha.helloworldopen.domain.Piece;
import br.com.dorinha.helloworldopen.domain.Track;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class CarPositionsMessageProcessor implements MessageTypeProcessor {

	private static final Logger logger = LoggerFactory.getLogger(CarPositionsMessageProcessor.class);

	@Override
	public void process(Game game, JsonObject json, ServerMessageType type) {
		game.waitConfigure();

		if (!json.has("data") || json.get("data").isJsonNull()) {
			logger.warn("Unexpected carPositions definition : {}", json);
			return;
		}

		JsonArray cars = json.get("data").getAsJsonArray();
		Iterator<JsonElement> iterator = cars.iterator();
		while (iterator.hasNext()) {
			JsonObject car = iterator.next().getAsJsonObject();
			processCar(game, car);
		}

		if (json.has("gameTick")) {
			game.tick(json.get("gameTick").getAsLong());
		}
	}

	private void processCar(Game game, JsonObject json) {
		if (!json.has("id")) {
			logger.warn("Unexpected car definition : {}", json);
			return;
		}

		JsonObject carJson = json.get("id").getAsJsonObject();
		if (!carJson.has("color")) {
			logger.warn("Unexpected car.id definition : {}", json);
			return;
		}

		Car car = game.getCar(carJson.get("color").getAsString());
		processCar(car, game.getTrack(), json);
	}

	void processCar(Car car, Track track, JsonObject json) {
		if (json.has("angle"))
			car.setAngle(json.get("angle").getAsDouble());

		if (json.has("piecePosition")) {
			JsonObject piecePosition = json.get("piecePosition").getAsJsonObject();
			if (!piecePosition.has("pieceIndex") || !piecePosition.has("lane") || !piecePosition.has("inPieceDistance")
					|| !piecePosition.has("lap")) {
				logger.warn("Unexpected piecePosition definition : {}", piecePosition);
				return;
			}

			Piece piece = track.getPieceOfIndex(piecePosition.get("pieceIndex").getAsInt());

			JsonObject lane = piecePosition.get("lane").getAsJsonObject();
			Lane startLane = track.getLaneOfIndex(lane.get("startLaneIndex").getAsInt());
			Lane endLane = track.getLaneOfIndex(lane.get("endLaneIndex").getAsInt());

			final double inPieceDistance = piecePosition.get("inPieceDistance").getAsDouble();
			final int lap = piecePosition.get("lap").getAsInt();

			car.setPiecePosition(piece, startLane, endLane, inPieceDistance, lap);
		}
	}
}
