package br.com.dorinha.helloworldopen.processor;

import br.com.dorinha.helloworldopen.domain.Car;
import br.com.dorinha.helloworldopen.domain.Game;
import com.google.gson.JsonObject;

public class LapFinishedMessageProcessor implements MessageTypeProcessor {

    @Override
    public void process(Game game, JsonObject json, ServerMessageType type) {
        final String color = json.get("data").getAsJsonObject().get("car").getAsJsonObject().get("color").getAsString();

        //{"msgType":"lapFinished","data":{"car":{"name":"dorinha","color":"red"},"lapTime":{"lap":0,"ticks":748,"millis":12467},"raceTime":{"laps":1,"ticks":749,"millis":12483},"ranking":{"overall":1,"fastestLap":1}},"gameId":"4e9c5762-8e8a-4cc4-b2a5-a65f3c218267","gameTick":749}

        long lapTime = json.get("data").getAsJsonObject().get("lapTime").getAsJsonObject().get("millis").getAsLong();
        int ranking = json.get("data").getAsJsonObject().get("ranking").getAsJsonObject().get("overall").getAsInt();

        final int tick;
        if (json.has("gameTick")) {
            tick = json.get("gameTick").getAsInt();
        } else {
            tick = -1;
        }

        Car car = game.getCar(color);
        car.lapFinished(lapTime);
        car.setRanking(ranking);

        if (tick >= 0) {
            game.tick(tick);
        }
    }
}
