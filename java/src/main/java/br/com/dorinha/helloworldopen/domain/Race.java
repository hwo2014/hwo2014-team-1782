package br.com.dorinha.helloworldopen.domain;

public class Race {

	private Track track;
	private int laps;
	private long maxLapTimeMs;
	private boolean quickRace;

	public Track track() {
		if (this.track == null) {
			this.track = new Track();
		}
		return track;
	}

	public void setLaps(int laps) {
		this.laps = laps;
	}

	public void setMaxLapTimeMs(long maxLapTimeMs) {
		this.maxLapTimeMs = maxLapTimeMs;
	}

	public void setQuickRace(boolean quickRace) {
		this.quickRace = quickRace;
	}

	public Track getTrack() {
		return track;
	}

	public int getLaps() {
		return laps;
	}

	public long getMaxLapTimeMs() {
		return maxLapTimeMs;
	}

	public boolean isQuickRace() {
		return quickRace;
	}
}
