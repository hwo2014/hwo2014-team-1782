package br.com.dorinha.helloworldopen.domain;

public class Lane {

	private double distanceFromCenter;
	private int index;

	public void setDistanceFromCenter(double distanceFromCenter) {
		this.distanceFromCenter = distanceFromCenter;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public double getDistanceFromCenter() {
		return distanceFromCenter;
	}

	public int getIndex() {
		return index;
	}
}
