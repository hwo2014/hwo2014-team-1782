package br.com.dorinha.helloworldopen.processor;

import br.com.dorinha.helloworldopen.domain.Game;

import com.google.gson.JsonObject;

public class GameStartMessageProcessor implements MessageTypeProcessor {

	@Override
	public void process(Game game, JsonObject json, ServerMessageType type) {
		if (json.has("gameTick")) {
			game.start(json.get("gameTick").getAsLong());
		} else {
			game.start();
		}
	}
}
